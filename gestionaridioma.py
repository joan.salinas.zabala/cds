
class GestionarIdioma:
    def __init__(self,basedatos):
        self.__basedatos=basedatos
    def agregarIdioma(self,idioma):
        insert= "INSERT INTO idioma(idioma) VALUES ('%s')"%(idioma.getIdioma())
        self.__basedatos.executeAndCommit(insert)

    def listarIdiomas(self):
        query =  "select * from idioma"
        return self.__basedatos.fetchAll(query)

    def eliminarIdioma(self, idioma):
        update="DELETE FROM idioma WHERE id=%s" % (str(idioma.getId()))
        self.__basedatos.executeAndCommit(update)

    def modificarIdioma(self,idioma):
        update="UPDATE idioma Set idioma = '%s' WHERE id= %i" % (idioma.getIdioma(), (idioma.getId()))
        self.__basedatos.executeAndCommit(update)
