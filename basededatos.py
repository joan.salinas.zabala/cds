import mysql.connector
class BaseDato:
    def __init__(self,user,password,host,database):
        self.__user=user
        self.__password=password
        self.__host=host
        self.__database=database
        self.__cnx=0
        self.__cursor=0
    def abrir(self):
        self.__cnx=mysql.connector.connect(user=self.__user,password=self.__password,host=self.__host,database=self.__database)
        self.__cursor=self.__cnx.cursor()
    def cerrar(self):
        self.__cursor.close()
        self.__cnx.close()
    def select(self,columna,tabla,condicion=0):
        if condicion==0:
            self.abrir()
            values=(columna,tabla)
            query =  "select %s from %s" % values
            self.__cursor.execute(query)#,values)
            x=self.__cursor.fetchall()
            self.cerrar()
            return x
        else:
            pass
    def getCursor(self):
        return self.__cursor
    def getCnx(self):
        return self.__cnx
    def executeAndCommit(self,instruccion):
        self.abrir()
        self.__cursor.execute(instruccion)
        self.__cnx.commit()
        self.cerrar()
    def fetchAll(self, instruccion):
        self.abrir()
        self.__cursor.execute(instruccion)
        values = self.__cursor.fetchall()
        self.cerrar()
        return values

