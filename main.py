from basededatos import BaseDato
from idioma import Idioma
from gestionaridioma import GestionarIdioma

miBaseDatos1=BaseDato("root","root","localhost","cd")
miIdioma1=Idioma("Catalan")
miGestionIdioma1=GestionarIdioma(miBaseDatos1)
#miGestionIdioma1.agregarIdioma(miIdioma1)

#print(miBaseDatos1.select("idioma","idioma"))
print(miGestionIdioma1.listarIdiomas())
miIdioma2=Idioma("Argentino",3)
miIdioma3=Idioma("Loquequieras",4)
miGestionIdioma1.modificarIdioma(miIdioma2)
miGestionIdioma1.eliminarIdioma(miIdioma3)